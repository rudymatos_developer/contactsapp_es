//
//  GenericException.swift
//  ContactsAppES
//
//  Created by Rudy E Matos on 1/16/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import Foundation

enum GenericException : Error{
    case fileNotFound(fileName: String)
    case errorParsingJSON(message: String)
    
}
