//
//  ContactAppImpl.swift
//  ContactsAppES
//
//  Created by Rudy E Matos on 1/15/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import Foundation
import CoreData

class ContactAppImpl : NSObject, ContactAppType{
    
    private let coreDataManager = CoreDataManager()
    private var contacts : [Contact]?
    private var filteredContacts : [Contact]?
    private var isFiltering  = false
    
    var sectionsTitles = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","#"]
    var selectedContact : Contact?
    
    private var isCreatingInitialLoad = false
    
    override init(){
        super.init()
        setupNotifications()
    }
    
    private func setupNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(ContactAppImpl.respondToObjectChanges), name: Notification.Name.NSManagedObjectContextObjectsDidChange, object: coreDataManager.managedObjectContext)
    }
    
    deinit{
        NotificationCenter.default.removeObserver(self, name: Notification.Name.NSManagedObjectContextObjectsDidChange, object: nil)
    }
    
    @objc func respondToObjectChanges(notification : Notification){
        if !isCreatingInitialLoad{
        guard let userInfo = notification.userInfo else {return}
        var contactChanged  = false
        
        if let inserts = userInfo[NSInsertedObjectsKey] as? Set<NSManagedObject>{
            for insert in inserts{
                if let contact = insert as? Contact{
                    contacts?.append(contact)
                    contactChanged = true
                }
            }
        }
        
        if let updates = userInfo[NSUpdatedObjectsKey] as? Set<NSManagedObject>{
            for update in updates{
                if let _ = update as? Contact{
                    contactChanged = true
                }
            }
        }
        
        if let deletes = userInfo[NSDeletedObjectsKey] as? Set<NSManagedObject>{
            for delete in deletes{
                if let contact = delete as? Contact, let index = contacts?.index(of: contact){
                    contacts?.remove(at: index)
                    contactChanged = true
                }
            }
        }
        
        if contactChanged{
            contacts = contacts?.sorted(by: {$0.getDisplayParam() < $1.getDisplayParam()})
            NotificationCenter.default.post(name: Notification.Name.ContactHasChanged, object: nil)
        }
        }
    }
    
    func getContacts() -> [Contact]{
        return isFiltering ? filteredContacts ?? [Contact]() : contacts ?? [Contact]()
    }
    
    func getSectionTitle(bySectionIndex : Int) -> String{
        return sectionsTitles[bySectionIndex]
    }
    
    func getHeightForHeaderInSection(section: Int) -> Int{
        return getContactsBySection(section: section).count > 0 ? 30 : 0
    }
    
    func getContact(bySection: Int, byRow: Int) -> Contact{
        return getContactsBySection(section: bySection)[byRow]
    }
    
    
    func getContactsBySection(section: Int) -> [Contact]{
        let sectionTitle = sectionsTitles[section]
        return getContacts().filter({ (contact) -> Bool in
            var validFilter = false
            if sectionTitle == "#"{
                if (!String.isStringNilOrEmpty(contact.lastName) && !String.startsWithValidCharacter(contact.lastName))
                    || (String.isStringNilOrEmpty(contact.lastName) && !String.isStringNilOrEmpty(contact.firstName) && !String.startsWithValidCharacter(contact.firstName))
                    || (String.isStringNilOrEmpty(contact.lastName) && String.isStringNilOrEmpty(contact.firstName)){
                    validFilter = true
                }
            }else {
                if contact.lastName?.uppercased().starts(with: sectionTitle) ?? false || (String.isStringNilOrEmpty(contact.lastName) && contact.firstName?.uppercased().starts(with: sectionTitle) ?? false) {
                    validFilter =  true
                }
            }
            return validFilter
        })
    }
    
    func filterContacts(byText text: String){
        if text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) != "" {
            filteredContacts = contacts?.filter({$0.filteringNamed().uppercased().contains(text.uppercased())})
            isFiltering = true
        }else{
            isFiltering = false
        }
    }
    
    func fetchContacts(){
        coreDataManager.managedObjectContext.performAndWait {
            do{
                let fetchRequest : NSFetchRequest = Contact.fetchRequest()
                contacts = try fetchRequest.execute()
                contacts = contacts?.sorted(by: {$0.getDisplayParam() < $1.getDisplayParam()})
            }catch{
                print("unable to retrieve objects from DB")
            }
        }
    }
    
    func deleteContact(contact: Contact){
        coreDataManager.managedObjectContext.delete(contact)
    }
    
    func createContact(firstName: String?, lastName : String?, phoneNumber: String? , streetAddress1: String?, streetAddress2: String?, city: String?, state: String? , zipCode: String?){
        let contact = Contact(context: coreDataManager.managedObjectContext)
        contact.contactID = UUID(uuidString: UUID().uuidString)
        contact.firstName = firstName
        contact.lastName = lastName
        contact.phoneNumber = phoneNumber
        contact.streetAddress1 = streetAddress1
        contact.streetAddress2 = streetAddress2
        contact.city = city
        contact.state = state
        contact.zipCode = zipCode
    }
    
    func persistInitialData(){
        do{
            if let filePath = Bundle.main.path(forResource: "contact_initial_data", ofType: "json"), let data = try String(contentsOfFile: filePath).data(using: .utf8){
                let jsonDecoder = JSONDecoder()
                let jsonContactObjectList = try jsonDecoder.decode([ContactTransferObject].self, from: data)
                isCreatingInitialLoad = true
                for currentJsonContactObject in jsonContactObjectList{
                    createContact(firstName: currentJsonContactObject.firstName, lastName: currentJsonContactObject.lastName, phoneNumber: currentJsonContactObject.phoneNumber, streetAddress1: currentJsonContactObject.streetAddress1, streetAddress2: currentJsonContactObject.streetAddress2, city: currentJsonContactObject.city, state: currentJsonContactObject.state, zipCode: currentJsonContactObject.zipCode)
                }
                coreDataManager.saveContext()
                isCreatingInitialLoad = false
            }
        }catch{
            print("Error, cannot initialize data. App will start with no records. Error Message: \(error)")
        }
    }
    
}
