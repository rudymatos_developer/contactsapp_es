//
//  String.swift
//  ContactsAppES
//
//  Created by Rudy E Matos on 1/17/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import Foundation

extension String{
    
    private static var validCharacters : [String] {
        return ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]
    }
    
    static func isStringNilOrEmpty( _ string: String?) -> Bool{
        guard let string = string else{
            return true
        }
        guard string.trimmingCharacters(in: .whitespacesAndNewlines) != "" else{
            return true
        }
        return false
    }
    
    static func startsWithValidCharacter(_ string: String?) -> Bool{
        if let string = string{
            for character in validCharacters{
                if string.uppercased().starts(with: character){
                    return true
                }
            }
            return false
        }else{
            return false
        }
    }
    
    
}
