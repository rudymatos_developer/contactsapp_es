//
//  UIViewController.swift
//  ContactsAppES
//
//  Created by Rudy E Matos on 1/16/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController{
    
    func setupContactObjectChangedNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(ContactInfoVC.updateContactObject), name: Notification.Name.ContactHasChanged, object: nil)
    }
    
}
