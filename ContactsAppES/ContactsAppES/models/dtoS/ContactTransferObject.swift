//
//  ContactTransferObject.swift
//  ContactsAppES
//
//  Created by Rudy E Matos on 1/15/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import Foundation

struct ContactTransferObject : Codable{
    let firstName: String
    let lastName : String
    let phoneNumber: String
    let streetAddress1 : String
    let streetAddress2: String
    let city : String
    let state: String
    let zipCode : String
}
