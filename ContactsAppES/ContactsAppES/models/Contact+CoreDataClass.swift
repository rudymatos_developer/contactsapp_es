//
//  Contact+CoreDataClass.swift
//  ContactsAppES
//
//  Created by Rudy E Matos on 1/15/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//
//

import Foundation
import CoreData

@objc(Contact)
public class Contact: NSManagedObject {
    static let entityName = "Contact"
    static let NONAME = "(NO NAME)"
    
    func getDisplayParam() -> String{
        if !String.isStringNilOrEmpty(lastName){
            return lastName!
        }
        if !String.isStringNilOrEmpty(firstName){
            return firstName!
        }
        if !String.isStringNilOrEmpty(phoneNumber){
            return phoneNumber!
        }
        return Contact.NONAME
    }
    
    func filteringNamed() -> String{
        let filteringString = "\(firstName ?? "") \(lastName ?? "") \(phoneNumber ?? "")"
        return filteringString.trimmingCharacters(in: .whitespacesAndNewlines) == "" ? Contact.NONAME : filteringString
    }
    
    func doesContactHaveAnyValue() -> Bool{
        return doesContactHaveName() || doesContactHaveAddress() || doesContactHavePhoneNumber()
    }
    
    func doesContactHaveName() -> Bool{
        return "\(firstName ?? "")\(lastName ?? "")".trimmingCharacters(in: .whitespacesAndNewlines) != ""
    }
    
    func doesContactHaveAddress() -> Bool{
        return "\(streetAddress1 ?? "")\(streetAddress2 ?? "")\(city ?? "")\(state ?? "")\(zipCode ?? "")".trimmingCharacters(in: .whitespacesAndNewlines) != ""
    }
    
    func doesContactHavePhoneNumber() -> Bool{
            return "\(phoneNumber ?? "")".trimmingCharacters(in: .whitespacesAndNewlines) != ""
        
    }
    
    func getFullName() -> String{
        return "\(firstName ?? "") \(lastName ?? "")"
    }
    
}
