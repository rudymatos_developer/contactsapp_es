//
//  AddEditContactVC.swift
//  ContactsAppES
//
//  Created by Rudy E Matos on 1/16/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import UIKit

enum Mode{
    case edit
    case create
}


class AddEditContactVC: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var firstNameTF: UITextField!
    @IBOutlet weak var lastNameTF: UITextField!
    @IBOutlet weak var phoneNumberTF: UITextField!
    @IBOutlet weak var streetName1TF: UITextField!
    @IBOutlet weak var streetName2TF: UITextField!
    @IBOutlet weak var cityTF: UITextField!
    @IBOutlet weak var zipCodeTF: UITextField!
    @IBOutlet weak var stateTF: UITextField!
    @IBOutlet weak var doneBTN: UIBarButtonItem!
    @IBOutlet weak var deleteContactBTN: UIButton!
    @IBOutlet weak var mainScrollView: UIScrollView!
    
    var contactAppImpl : ContactAppImpl?
    
    var mode: Mode = .create
    private var currentField: UITextField?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }
    
    //MARK: - VIEW METHODS
    
    @IBAction func updateContact(_ sender: UIBarButtonItem) {
        if mode == .edit{
            if let contact = contactAppImpl?.selectedContact{
                contact.firstName = firstNameTF?.text
                contact.lastName = lastNameTF?.text
                contact.phoneNumber = phoneNumberTF?.text
                contact.streetAddress1 = streetName1TF?.text
                contact.streetAddress2 = streetName2TF?.text
                contact.city = cityTF?.text
                contact.zipCode = zipCodeTF?.text
                contact.state = stateTF?.text
            }
        }else{
            contactAppImpl?.createContact(firstName: firstNameTF.text, lastName: lastNameTF.text, phoneNumber: phoneNumberTF.text, streetAddress1: streetName1TF.text, streetAddress2: streetName2TF.text, city: cityTF.text, state: stateTF.text, zipCode: zipCodeTF.text)
        }
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func deleteContact(_ sender: UIButton) {
        if let contact = contactAppImpl?.selectedContact{
            let controller = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            let deleteAction = UIAlertAction(title: "Delete Contact", style: .destructive) { (action) in
                self.contactAppImpl?.deleteContact(contact: contact)
                self.performSegue(withIdentifier: "goBackToMainScreen", sender: nil)
            }
            controller.addAction(deleteAction)
            controller.addAction(cancelAction)
            present(controller, animated: true, completion: nil)
        }
    }
    
    func configureView(){
        if mode == .edit{
            navigationItem.title = ""
            if let contact = contactAppImpl?.selectedContact{
                firstNameTF?.text = contact.firstName
                lastNameTF?.text = contact.lastName
                phoneNumberTF?.text = contact.phoneNumber
                streetName1TF?.text = contact.streetAddress1
                streetName2TF?.text = contact.streetAddress2
                cityTF?.text = contact.city
                zipCodeTF?.text = contact.zipCode
                stateTF?.text = contact.state
            }
        }else{
            doneBTN.isEnabled = false
            deleteContactBTN.isHidden = true
        }
        registerKeyboardNotification()
    }
    
    @IBAction func didUpdateContact(_ sender: UITextField) {
        doneBTN.isEnabled = firstNameTF?.text?.trimmingCharacters(in: .whitespacesAndNewlines) != "" ||
            lastNameTF?.text?.trimmingCharacters(in: .whitespacesAndNewlines) != "" ||
            phoneNumberTF?.text?.trimmingCharacters(in: .whitespacesAndNewlines) != "" ||
            streetName1TF?.text?.trimmingCharacters(in: .whitespacesAndNewlines) != "" ||
            streetName2TF?.text?.trimmingCharacters(in: .whitespacesAndNewlines) != "" ||
            cityTF?.text?.trimmingCharacters(in: .whitespacesAndNewlines) != "" ||
            zipCodeTF?.text?.trimmingCharacters(in: .whitespacesAndNewlines) != "" ||
            stateTF?.text?.trimmingCharacters(in: .whitespacesAndNewlines) != ""
    }
    
   
    @IBAction func dismissVC(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - UITEXTFIELD DELEGATE
    
    @objc fileprivate func keyboardWillHide(notification: Notification){
        mainScrollView.contentInset = UIEdgeInsets.zero
        mainScrollView.scrollIndicatorInsets = UIEdgeInsets.zero
    }
    
    fileprivate func registerKeyboardNotification(){
        NotificationCenter.default.addObserver(self, selector: #selector(AddEditContactVC.keyboardDidShow(notification:)), name: Notification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(AddEditContactVC.keyboardWillHide(notification:)), name: Notification.Name.UIKeyboardWillHide, object: nil)
    }
    
    @objc fileprivate func keyboardDidShow(notification: Notification){
        if let currentField = currentField{
            if let userInfo = notification.userInfo as NSDictionary?{
                let kbSize = (userInfo.object(forKey: UIKeyboardFrameEndUserInfoKey) as! CGRect).size
                let contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0)
                mainScrollView.contentInset  = contentInsets
                mainScrollView.scrollIndicatorInsets = contentInsets
                var rect = self.view.frame
                rect.size.height -= (kbSize.height)
                if !rect.contains(currentField.frame.origin){
                    mainScrollView.scrollRectToVisible(currentField.frame, animated: true)
                }
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.returnKeyType == .done{
            textField.resignFirstResponder()
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        currentField = textField
    }
    
    //MARK: -DEINIT
    deinit {
        NotificationCenter.default.removeObserver(self, name: Notification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name.UIKeyboardWillHide, object: nil)
    }
    
}


