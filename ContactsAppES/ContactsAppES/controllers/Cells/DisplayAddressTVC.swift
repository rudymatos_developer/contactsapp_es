//
//  DisplayAddressTVC.swift
//  ContactsAppES
//
//  Created by Rudy E Matos on 1/16/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import UIKit

class DisplayAddressTVC: UITableViewCell {
    @IBOutlet weak var streetName1LBL: UILabel!
    @IBOutlet weak var streetName2LBL: UILabel!
    @IBOutlet weak var cityLBL: UILabel!
    @IBOutlet weak var zipCodeLBL: UILabel!
    @IBOutlet weak var stateLBL: UILabel!
    
    var contact: Contact?{
        didSet{
            configureView()
        }
    }
    
    func configureView(){
        if let contact = contact{
            streetName1LBL.text = contact.streetAddress1
            streetName2LBL.text = contact.streetAddress2
            cityLBL.text = contact.city
            zipCodeLBL.text = contact.zipCode
            stateLBL.text = contact.state
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
