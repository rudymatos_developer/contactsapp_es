//
//  DisplayPhoneTVC.swift
//  ContactsAppES
//
//  Created by Rudy E Matos on 1/16/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import UIKit

class DisplayPhoneTVC: UITableViewCell {
    
    @IBOutlet weak var phoneNumberLBL: UILabel!
    
    var contact: Contact?{
        didSet{
            configureView()
        }
    }
    
    func configureView(){
        if let contact = contact {
            phoneNumberLBL.text = contact.phoneNumber
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
