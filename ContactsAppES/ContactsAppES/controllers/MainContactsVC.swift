//
//  MainContactsVC.swift
//  ContactsAppES
//
//  Created by Rudy E Matos on 1/15/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import UIKit

class MainContactsVC: UITableViewController, UISearchResultsUpdating {
    
    private let contactAppImpl = ContactAppImpl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if #available(iOS 11, *){
            navigationItem.hidesSearchBarWhenScrolling = false
        }
    }
    
    fileprivate func configureView(){
        isFirstLaunch()
        contactAppImpl.fetchContacts()
        setupSearchController()
        setupContactObjectChangedNotifications()
    }
    
    private func isFirstLaunch(){
        if !UserDefaults.standard.bool(forKey: "isFirstLaunch"){
            contactAppImpl.persistInitialData()
            UserDefaults.standard.set(true, forKey: "isFirstLaunch")
            UserDefaults.standard.synchronize()
        }
    }
    
    private func setupSearchController(){
        let searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search"
        navigationItem.searchController = searchController
        definesPresentationContext = true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "contactInfoSegue"{
            guard let contactInfoVC = segue.destination as? ContactInfoVC else{
                fatalError("invalid segue")
            }
            contactInfoVC.contactAppImpl = contactAppImpl
        }else if segue.identifier == "createNewContactSegue",let editNavigationVC = segue.destination as? UINavigationController, let editContactInfoVC  = editNavigationVC.viewControllers.first as? AddEditContactVC{
            editContactInfoVC.mode = .create
            editContactInfoVC.contactAppImpl = contactAppImpl
        }
    }
    
    @IBAction func afterDeletingContact(storyboard: UIStoryboardSegue){
    }
    
    @objc func updateContactObject(){
        tableView.reloadData()
    }

    //MARK: - Search Results
    func updateSearchResults(for searchController: UISearchController) {
        if let text = searchController.searchBar.text{
            contactAppImpl.filterContacts(byText: text)
            self.tableView.reloadData()
        }
    }

    
    
    
    //MARK: - UITableview Delegate and Datasource
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        contactAppImpl.selectedContact = contactAppImpl.getContact(bySection: indexPath.section, byRow: indexPath.row)
        tableView.deselectRow(at: indexPath, animated: false)
        performSegue(withIdentifier: "contactInfoSegue", sender: nil)
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return contactAppImpl.sectionsTitles.count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return contactAppImpl.sectionsTitles[section]
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contactAppImpl.getContactsBySection(section: section).count
    }
    
    override func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int {
        return contactAppImpl.sectionsTitles.index(of: title) ?? 0
    }
    
    override func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return contactAppImpl.sectionsTitles
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.init(contactAppImpl.getHeightForHeaderInSection(section: section))
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let currentContact = contactAppImpl.getContact(bySection: indexPath.section, byRow: indexPath.row)
        if  let cell = tableView.dequeueReusableCell(withIdentifier: "contact_cell"){
            cell.textLabel?.text = currentContact.doesContactHaveName() ? currentContact.getFullName() : Contact.NONAME
            cell.detailTextLabel?.text = currentContact.phoneNumber
            return cell
        }else{
            return UITableViewCell()
        }
    }
    
    //MARK: - deinit
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: Notification.Name.ContactHasChanged, object: nil)
    }
    
}
