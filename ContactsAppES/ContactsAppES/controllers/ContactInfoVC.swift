//
//  ContactInfoVC.swift
//  ContactsAppES
//
//  Created by Rudy E Matos on 1/16/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import UIKit

class ContactInfoVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var contactInfoEmptyLBL: UILabel!
    
    var contactAppImpl : ContactAppImpl?{
        didSet{
            setupContactObjectChangedNotifications()
        }
    }
    
    func configureView(){
        if let selectedContact = contactAppImpl?.selectedContact{
            navigationItem.title = selectedContact.doesContactHaveName() ? selectedContact.getFullName() : Contact.NONAME
            if !selectedContact.doesContactHaveAddress() && !selectedContact.doesContactHavePhoneNumber(){
                tableView.isHidden = true
                contactInfoEmptyLBL.isHidden = false
            }else{
                tableView.isHidden = false
                contactInfoEmptyLBL.isHidden = true
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "editContactSegue", let editNavigationVC = segue.destination as? UINavigationController, let editContactInfoVC  = editNavigationVC.viewControllers.first as? AddEditContactVC{
            editContactInfoVC.mode = .edit
            editContactInfoVC.contactAppImpl = contactAppImpl
        }
    }
    
    
    @objc func updateContactObject(){
        configureView()
        tableView.reloadData()
    }
    
    
    //MARK: - UITABLEVIEW DATASOURCE AND DELEGATE
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height = 0
        guard let contact = contactAppImpl?.selectedContact else{
            return 0
        }
        if  indexPath.section == 0 {
            height = contact.doesContactHavePhoneNumber() ? 70 : 0
        }else{
            height = contact.doesContactHaveAddress() ? 165 : 0
        }
        return CGFloat.init(height)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let contact = contactAppImpl?.selectedContact else {
            return UITableViewCell()
        }
        if indexPath.section == 0, contact.doesContactHavePhoneNumber(), let cell  = tableView.dequeueReusableCell(withIdentifier: "displayPhoneTVC") as? DisplayPhoneTVC{
            cell.contact = contact
            return cell
        }else if indexPath.section == 1, contact.doesContactHaveAddress(),let cell  = tableView.dequeueReusableCell(withIdentifier: "displayAddressTVC") as? DisplayAddressTVC{
            cell.contact = contact
            return cell
        }
        return UITableViewCell()
    }
    
    //MARK: - DEINIT
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: Notification.Name.ContactHasChanged, object: nil)
    }
   
}

extension ContactInfoVC{
    
    
}


