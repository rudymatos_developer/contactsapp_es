//
//  CoreDataHelper.swift
//  ContactsAppES
//
//  Created by Rudy E Matos on 1/15/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import Foundation
import CoreData
import UIKit

final class CoreDataManager{
    
    let managedObjectContext : NSManagedObjectContext
    
    init(){
        managedObjectContext = persistentContainer.viewContext
        setupNotifications()
    }
    
    var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "ContactsAppES")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    @objc func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            context.performAndWait {
                do {
                    try context.save()
                } catch {
                    let nserror = error as NSError
                    fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
                }
                
            }
        }
    }
    
    
    func setupNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(CoreDataManager.saveContext), name: Notification.Name.UIApplicationWillTerminate, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(CoreDataManager.saveContext), name: Notification.Name.UIApplicationDidEnterBackground, object: nil)
    }
    
}
