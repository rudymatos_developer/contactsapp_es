//
//  ContactAppType.swift
//  ContactsAppES
//
//  Created by Rudy E Matos on 1/15/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import Foundation

protocol ContactAppType{
    
    var selectedContact: Contact? { get set }
    var sectionsTitles : [String] {get}
    
    func createContact(firstName: String?, lastName : String?, phoneNumber: String? , streetAddress1: String?, streetAddress2: String?, city: String?, state: String? , zipCode: String?)
    func persistInitialData()
    func getContacts() -> [Contact]
    func getSectionTitle(bySectionIndex : Int) -> String
    func getHeightForHeaderInSection(section: Int) -> Int
    func getContact(bySection: Int, byRow: Int) -> Contact
    func getContactsBySection(section: Int) -> [Contact]
    func filterContacts(byText text: String)
    func fetchContacts()
    func deleteContact(contact: Contact)
}
